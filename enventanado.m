%clear
%clc
function salida = enventanado(cancion,tamano_ventana)
  %cancion='a-team_intro.wav';
  %salida=[1;2;3;4;5;6;7]
  %tamano_ventana=2;
  %salida=audioread(cancion);
  ventanado=cancion';
  %largo=length(salida);
  numero_ventanas=floor(length(cancion)/tamano_ventana);
  
  for i = 1:1:numero_ventanas
    final=i*tamano_ventana;
    ventanado(i,(1):tamano_ventana)=ventanado(1,(final-tamano_ventana+1):final);
  endfor
  
  ventana_final=length(cancion)-final;
  if(ventana_final!=0)
    ventanado((i+1),(1):ventana_final)=ventanado(1,(final+1):(final+ventana_final));
  endif
  
  salida=ventanado([1:(i+1)],1:tamano_ventana);
  
endfunction