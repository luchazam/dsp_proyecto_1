function salida = enventanado_vector(cancion,tamano_ventana)
  ventanado=cancion';
  numero_ventanas=floor(length(cancion)/tamano_ventana);
    
  for i = 1:1:numero_ventanas
    final=i*tamano_ventana;
    salida(1,i)=ventanado(1,final);
  endfor

endfunction