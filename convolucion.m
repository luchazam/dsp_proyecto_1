function salida = convolucion(ventana,retardo,atenuacion)
  eco=zeros(1,retardo);
  %Se coloca al primer valor
  eco(1)=1;
  %Se coloca el ultimo valor
  eco(retardo)=atenuacion;
  salida = conv(ventana,eco);

endfunction